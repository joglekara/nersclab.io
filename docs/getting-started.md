# Getting Started

!!! success "About this page"
	This document will guide you through the basics of using NERSC's
	supercomputers, storage systems, and services.

Welcome to the National Energy Research Scientific Computing Center
(NERSC)!

* Talk *NERSC Overview* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/01-Overview-2022.pdf),
  [Video](https://www.youtube.com/watch?v=jnREu83uc88&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

## Computing Resources

### Perlmutter

Perlmutter is a [HPE Cray
EX](https://support.hpe.com/hpesc/public/docDisplay?docId=a00109703en_us&docLocale=en_US)
supercomputer with over 1500 GPU-accelerated compute nodes.

* [Perlmutter system information](./systems/perlmutter/index.md)
* [NERSC Live status](https://www.nersc.gov/live-status/motd/)

## Storage Resources

File systems are configured for different purposes. Perlmutter has access
to at least three different file systems with different levels of performance,
permanence and available space.

* [NERSC File System Overview](filesystems/index.md)
* Talk *Data Storage & Sharing Best Practices* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/09-Data-Storage-and-Sharing-2022.pdf),
  [Video](https://www.youtube.com/watch?v=Z5C4LdJoMr0&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)
* Talk *I/O Best Practices* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/10-IO-Best-Practices-2022.pdf),
  [Video](https://www.youtube.com/watch?v=OimM9Xnwjpc&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

### Community File System (CFS)

The [Community File System (CFS)](./filesystems/community.md) is a
global file system available on all NERSC computational and data transfer systems.
It allows sharing of data between users, systems, and the "outside
world".

* [Detailed CFS usage information](./filesystems/community.md)

### High Performance Storage System (HPSS) Archival Storage

The High Performance Storage System (HPSS) is a modern, flexible,
performance-oriented mass storage system. It has been used at NERSC
for archival storage since 1998. HPSS is intended for long term
storage of data that is not frequently accessed.

* [Detailed HPSS usage information](./filesystems/archive.md)

## NERSC Accounts

In order to use the NERSC facilities, you need to [obtain a NERSC account](./accounts/index.md#obtaining-an-account)
and your user account must be tied to a [NERSC Allocation](https://www.nersc.gov/users/accounts/allocations/) in order
to run jobs. Please see our [password rules](./accounts/passwords.md) if you need help creating account
or recovering passwords. Once you have an account, you can login to [Iris](https://iris.nersc.gov) to manage
account details.

For more details on account support see following links below:

* Talk *Accounts and Allocations* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/02-AccountsAllocations-2022.pdf),
  [Video](https://www.youtube.com/watch?v=iZLHffA_QtY&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

!!! tip "With [Iris](https://iris.nersc.gov) you can"
	* Check allocation balances
	* Change passwords
	* Run reports
	* Update contact information
	* Clear login failures
	* Change login shell
	* and more!

## Email

When you get a NERSC user account, an email alias is created for you
of the form `<username>@nersc.gov` that will forward email to your
delivery address (set in [Iris](https://iris.nersc.gov)).

!!! note "NERSC users email list"
	As a NERSC user you will be subscribed to the NERSC users
	email list. Users cannot unsubscribe from this list due to the
	imporant nature of the information.

NERSC provides several other email lists, including one with
additional system status details. [NERSC Email
lists](https://www.nersc.gov/news-publications/announcements/email-lists/).

## Connecting to NERSC

!!! Check "MFA is *required* for NERSC users"

* [Multi-Factor Authentication (MFA)](./connect/mfa.md)
* [SSH](./connect/index.md#ssh)
* [Login nodes](./connect/index.md)
* [Troubleshooting connection problems](./connect/index.md#troubleshooting)
* [NERSC Live status](https://www.nersc.gov/live-status/motd/)
* Talk *Navigating NERSC* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/03-Navigating-NERSC-2022.pdf),
  [Video](https://www.youtube.com/watch?v=q-PKIR1FUPg&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

## Software

NERSC and its vendors supply a rich set of compilers, HPC utilities,
programming libraries, development tools, debuggers/profilers, data and visualization tools.

We provide a list of [applications](./applications/index.md) that are built
by NERSC staff with links to documentation for each. An increasing portion 
of the NERSC software stack is
being provided via the [Spack](./development/build-tools/spack.md)
package manager, which helps automate software builds for popular HPC
tools.

Software can be accessed via the `module` command. Currently we support [lmod](./environment/lmod.md)
for Perlmutter.

Please see our [Software Policy](./policies/software-policy/index.md) outlining our software support model
for the NERSC provided software stack.

!!! question "Something missing?"
	If there is something missing that you would like to have on
	our systems, please [submit a request](https://help.nersc.gov) and
	we will evaluate it for appropriateness, cost, effort, and benefit
	to the community.

### Python

The Python we provide at NERSC is [Anaconda Python](https://docs.anaconda.com/anaconda/user-guide/getting-started/).

* [Brief introduction to Python at NERSC](development/languages/python/index.md)
* Talk *Python at NERSC* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/11-Python-at-NERSC-2022.pdf),
  [Video](https://www.youtube.com/watch?v=vCQw1Zmncug&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

### Machine Learning

NERSC supports a variety of software for machine learning and deep learning on our systems.

* [Machine Learning at NERSC](machinelearning/index.md)
* Talk *Deep Learning at NERSC* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/14-Deep-Learning-at-NERSC-2022.pdf),
  [Video](https://www.youtube.com/watch?v=2IfiCba4n6E&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

## Computing Environment

!!! info
	[`$HOME` directories](./filesystems/global-home.md) are shared
	across all NERSC systems (except HPSS)

## Compiling/ building software

* [Compilers at NERSC](./development/compilers/wrappers.md)
* [Shell Environment](./environment/index.md)
* Talk *Programming Environment and Compilation* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/04-PE-and-Compilation-2022.pdf),
  [Video](https://www.youtube.com/watch?v=kKkXSLaIjT8&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

## Running Jobs

Typical usage of the system involves submitting scripts (also
referred to as "jobs") to a batch system such as
[Slurm](https://slurm.schedmd.com/).

* [Overview of jobs at NERSC](./jobs/index.md)
* [Rich set of example jobs](./jobs/examples/index.md)
* Talk *Running Jobs* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/05-Running-Jobs-2022.pdf),
  [Video](https://www.youtube.com/watch?v=eI47OtFZR0s&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)
* Talk *Workflows at NERSC* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/08-Workflows-at-NERSC-2022.pdf),
  [Video](https://www.youtube.com/watch?v=vtKHwF9tHhk&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

## Interactive Computing

NERSC provides interactive computing via [interactive slurm jobs](./jobs/interactive.md) 
and the [Jupyter](services/jupyter/index.md) service.

* Talk *Jupyter at NERSC* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/12-Using-Jupyter-at-NERSC-2022.pdf),
  [Video](https://www.youtube.com/watch?v=BxOfh7SEYC4&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

## Debugging and Profiling

NERSC provides many popular debugging and profiling tools.

* [Performance Tools](tools/performance/index.md)
* [Debugging Tools](tools/debug/index.md)
* Talk *Debugging Tools* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/07-Debugging-Tools-2022.pdf),
  [Video](https://www.youtube.com/watch?v=1a6aweOdt7Q&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

## Data Sharing

### Security and Data Integrity

Sharing data with other users must be done carefully. Permissions
should be set to the minimum necessary to achieve the desired
access. For instance, consider carefully whether it's really necessary
before sharing write permissions on data. Be sure to have archived
backups of any critical shared data. It is also important to ensure
that private login secrets (like SSH private keys or apache
htaccess files) are not shared with other users (either intentionally
or accidentally). Good practice is to keep things like this in a
separate directory that is as locked down as possible.

### Sharing with Other Members of Your Project

NERSC's [Community file system](filesystems/community.md) is set up
with group read and write permissions and is ideal for sharing with
other members of your project. There is a directory for every active
project at NERSC and all members of that project should have access to
it by default.

### Sharing with NERSC Users Outside of Your Project

You can share files and directories with NERSC users outside of your
project by adjusting the unix file permissions. We have an [extensive
write-up of unix file
permissions](filesystems/unix-file-permissions.md) and how they work.

NERSC provides two commands: `give` and `take` which are useful for
sharing *small amounts* of data between users.

To send a file or path to `<receiving_username>`:

```
give -u <receiving_username> <file or directory>
```

To receive a file sent by `<sending_username>`:

```
take -u <sending_username> <filename>
```

To take all files from `<sending_username>`:

```
take -a -u <sending_username>
```

To see what files `<sending_username>` has sent to you:

```
take -u <sending_username>
```

For a full list of options pass the `--help` flag.

!!! warning
    Files that remain untaken 12 weeks after being given will be
    purged from the staging area.

### Sharing Data outside of NERSC

You can easily and quickly share data over the web using our [Science
Gateways](services/science-gateways.md) framework.

You can also share large volumes of data externally by setting up a
[Globus Sharing Endpoint](services/globus.md#globus-sharing).

## Data Transfers

NERSC partners with ESNet to provide a high-speed connection to the
outside world. NERSC also provides several tools and systems optimized
for data transfer.

* Talk *Data Storage & Sharing Best Practices* at New User Training event, September 28, 2022 -
  [Slides](https://www.nersc.gov/assets/Uploads/09-Data-Storage-and-Sharing-2022.pdf),
  [Video](https://www.youtube.com/watch?v=Z5C4LdJoMr0&list=PL20S5EeApOStKCZj82t8KEQ7bGeNkI8E4)

### External Data Transfer

!!! tip
    **NERSC recommends transferring data to and from
    NERSC using [Globus](services/globus.md)**

[Globus](services/globus.md) is a web-based service that solves
many of the challenges encountered moving data between systems. Globus
provides the most comprehensive, efficient, and easy to use
service for most NERSC users.

However, there are other tools available to transfer data between
NERSC and other sites:

* [scp](services/scp.md): standard Linux utilities suitable for smaller files (<1GB)
* [GridFTP](services/gridftp.md): parallel transfer software for large files

### Transferring Data Within NERSC

!!! tip
    **"Do you need to transfer at all?"**  If your data is on NERSC
    Global File Systems ([Community](./filesystems/community.md),
    [Common](./filesystems/global-common.md),
    [Home](./filesystems/global-home.md)) transfering data may not be
    necessary because these file systems are mounted on almost all NERSC
    systems.  
    However, if you are doing a lot of I/O with these files,
    you will benefit from staging them on the most performant file
    system. Usually that's the local [Scratch](./filesystems/index.md#scratch)
    (pscratch) file system, which is only mounted on
    Perlmutter.

* Use the the unix command `cp`, `tar` or `rsync` to copy files within
   the same computational system. For large amounts of data use Globus
   to leverage the automatic retry functionality.

### Data Transfer Nodes

The [Data Transfer Nodes (DTNs)](systems/dtn/index.md) are servers
dedicated for data transfer based upon the ESnet Science DMZ
model. DTNs are tuned to transfer data efficiently, are optimized for
bandwidth, and have direct access to most of the NERSC file
systems. These transfer nodes are configured within Globus as managed
endpoints available to all NERSC users.

### NERSC FTP Upload Service

NERSC maintains [an FTP upload
service](https://rest.nersc.gov/REST/ftp_form/form.html)
designed for external collaborators to be able to send data to NERSC
staff and users.

## Getting Help

NERSC places a very strong emphasis on enabling science and providing
user-oriented systems and services. If you require additional support we
encourage you to search [our documentation](index.md) for a solution before
opening a ticket.

### Help Desk

!!! info "Availability"
	Account support is available 8-5 Pacific Time on business days.

The [online help desk](https://help.nersc.gov/) is the **preferred**
method for contacting NERSC.

!!! help "Before you open a ticket"
    * [Password resets](accounts/passwords.md) can be done automatically
       without opening a ticket!
    * We encourage you to search [our documentation](index.md) before
      opening a ticket.
    * New users should read this [Getting started guide](getting-started.md).
    * Account changes can be processed through
      [Iris](https://iris.nersc.gov) ([Iris guide for
      users](iris/iris-for-users.md)).

### How to File a Good Ticket

NERSC Consultants handle thousands of support requests per
year. In order to ensure efficient timely resolution of your issue,
include **as much of the following as possible** when making a
request:

* error messages
* jobids
* location of relevant files
    * input/output
    * job scripts
    * source code
    * executables
* output of `module list`
* any steps you have tried
* steps to reproduce

Please copy and paste any text directly into the ticket and only
include screenshots as attachements when the graphical output is
the subject of the support request.

!!! tip
    You can make code snippets, shell outputs, etc in your ticket much more
    readable by inserting a line with:

    ```
    [code]<pre>
    ```

    before the snippet, and another line with:

    ```
    </pre>[/code]
    ```

    after it. While these are the most useful, other options to improve
    formatting can be found in the [full list of formatting
    options](https://community.servicenow.com/community?id=community_blog&sys_id=4d9ceae1dbd0dbc01dcaf3231f9619e1).

Access to the online help system requires logging in with your NERSC username,
password, and one-time password. If you are an existing user unable to log in,
you can send an email to <accounts@nersc.gov> for support.

If you are not a NERSC user, you can reach NERSC with your queries at
`accounts@nersc.gov` or `allocations@nersc.gov`.

### Operations

!!! warning "For *critical* system issues only."

* Please check the [Online Status Page](https://www.nersc.gov/live-status/motd/)
 before calling 1-800-666-3772 (USA only) or 1-510-486-8600, Option 1

### Phone support

**Consulting and account-support phone services have been suspended.**

To report an urgent system issue, you may call NERSC at 1-800-66-NERSC
(USA) or 510-486-8600 (local and international).

### FAQ

Please see the following links for common issues that can be addressed, if you 
are still having issues, please create a ticket in [help desk](https://help.nersc.gov).

* [Password Resets](accounts/passwords.md#forgotten-passwords)
* [Connection problems](connect/index.md#ssh)
* [File Permissions](filesystems/unix-file-permissions.md)
* [Troubleshooting Slurm](jobs/troubleshooting.md)

### Appointments with NERSC User-Support Staff

NERSC provides 30-minute appointments with NERSC expert staff.  Before
you [schedule your appointment](https://nersc.as.me) consult the list
of available topics described below.

!!! tip
    To make the most use of an appointment, we **strongly
    encourage** you to try some things on your own and share them with
    NERSC staff ahead of time using the appointment intake form.

#### NERSC 101

This category is good for basic questions, and you could not find the answer in
our documentation. Or when you just don't know where to start.

#### Containers at NERSC

Advice on deploying containerized workflows at NERSC using Shifter. We
recommend that you download your image to Perlmutter (see [How to Use
Shifter](development/shifter/how-to-use.md)) before the appointment if possible,
and be prepared to share your Dockerfile and the image name.

#### NERSC Filesystems

Advice on I/O optimization and Filesystems at NERSC. Possible discussion topics
include:

1. Optimal file system choices
2. Quota and file-permission issues
3. I/O profiling
4. Refactoring your code

#### GPU Basics

Advice on programming GPUs for users that are new to the topic. This category
is good for when you have started developing your GPU code, but are
encountering problems.

#### Using GPUs in Python

Advice on how to use GPUs from Python, eg. `CuPy`, `RAPIDS`, or `PyCUDA`.

#### Checkpoint/Restart using MANA

Advice on how to use MANA to enable automatic
[checkpoint/restart](development/checkpoint-restart/index.md) in
MPI applications.
