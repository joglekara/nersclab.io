# Machine Learning at NERSC

NERSC supports a variety of software for Machine Learning and Deep Learning
on our systems.

These docs include details about how to use our system optimized frameworks,
multi-node training libraries, and performance guidelines.

## Classical Machine Learning

Libraries like scikit-learn and other non-deep-learning libraries are supported
through our standard installations and environments for Python and R, more under
[Analytics](../analytics/analytics.md).

## Deep Learning Frameworks

We have prioritized support for the following Deep Learning frameworks:

* [TensorFlow](tensorflow.md)
* [PyTorch](pytorch.md)

## Deploying with Jupyter

Users can deploy distributed deep learning workloads from Jupyter
notebooks using parallel execution libraries such as IPyParallel. Jupyter
notebooks can be used to submit workloads to the batch system and also
provide powerful interactive capabilities for monitoring and controlling those
workloads.

## Benchmarks

We track general performance of Deep Learning frameworks as well as some
specific scientific applications. See the [benchmarks](benchmarks.md) for details.

## Science use-cases

Machine Learning and Deep Learning are increasingly used to analyze
scientific data, in diverse fields. We have gathered some examples of
work ongoing at NERSC on
the [science use-cases page](science-use-cases/index.md) including some code
and datasets and how to run these at NERSC.
