# ORCA

[ORCA](https://www.faccts.de/orca/) is an ab initio quantum chemistry program
package that contains modern electronic structure methods including density
functional theory, many-body perturbation, coupled cluster, multireference
methods, and semi-empirical quantum chemistry methods. Its main field of
application is larger molecules, transition metal complexes, and their
spectroscopic properties. ORCA is developed in the research group of Frank
Neese. The free version is available only for academic use at academic
institutions.

From [Wikipedia](https://en.wikipedia.org/wiki/ORCA_(quantum_chemistry_program))

!!! Attention
    ORCA requires all users to create an account on their forum board before
    executables can be downloaded. Moreover, each executable is linked to a
    specific version of OpenMPI, so a containerized solution is usually
    ideal for running on Perlmutter.
  
    [Link to Orca Forum](https://orcaforum.kofo.mpg.de/app.php/portal)

## Support

ORCA user can seek support via the [Orca forum](https://orcaforum.kofo.mpg.de/app.php/portal).

## Containers for ORCA

| OpenMPI ver. | Repository                 | ORCA version |
| :----------: | :-----------------------:  | :----------: |
| 4.1.1        | docker.io/stephey/orca:3.0 | 5.0.4        |

## Steps to Install

The following directions explain how to do a test install on a user's
scratch file space, `PSCRATCH`. For long term use and to avoid having
the executables purged, ORCA should be installed or moved to a proper
location such as `/global/common/software`.

### Obtain Container Image

Define a place for podman-hpc to store images.

```
mkdir $PSCRATCH/storage
```

Pull the container image from Docker.

```
podman-hpc pull docker.io/stephey/orca:3.0
```

Show the available images.

```
podman-hpc images
```

### Obtain ORCA Executables

Create an account in the forum and download from the icon at the top of the
page. These directions were tested with the static MPI version.

Make a folder to hold the ORCA executables.

```
mkdir $PSCRATCH/orca
```

Download tarballs (available by signing up at the [forum](https://orcaforum.kofo.mpg.de/))
to this directory and decompress them with `tar -xvf`.
Move all the executables into the main directory. `cd` into the directory
and then `mv ../*` to move the files back. Now the ORCA directory is setup.

## Steps to Run

### Interactive Session

Start an interactive session.

```
salloc <job parameters>
```

Then,

```
podman-hpc run --rm -it --volume=$SCRATCH/orca:/orca --workdir=/orca docker.io/stephey/orca:3.0
```

This runs the container interactively and mounts the ORCA directory at `/orca`.
The container will use `/orca` as the working directory and the `--rm -it` flags,
tell podman-hpc to remove the container after it closes and to set it for
interactive use, respectively.

To run ORCA with the input file, `input.inp`, type,

```
./orca input.inp
```

To run orca with multiple MPI processes we need to set the environment variables:

```
export OMPI_ALLOW_RUN_AS_ROOT=1
export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
```

Then run your input file with the required ORCA directives to make it run in
parallel. For example,

```
./orca input_parallel.inp
```

### Job Submission Script

The code snippet below is an example batch job that uses podman-hpc to load the
ORCA container, and then runs a multi-MPI-process calculation on the input file,
`input_parallel.inp`.

```
#!/bin/bash                                                                     
                                                                                   
#SBATCH --qos regular                                                              
#SBATCH --constraint cpu                                                        
#SBATCH --nodes 1                                                               
#SBATCH --time 10                                                               
#SBATCH --account <account>                                                        
#SBATCH --image=docker:stephey/orca:3.0                                         
#SBATCH --volume="/pscratch/sd/e/elvis/ORCA:/ORCA"                       
                                                                                
podman-hpc run \                                                                
  --rm \                                                                        
  --volume=/pscratch/sd/e/elvis/ORCA:/ORCA \                             
  --workdir=/ORCA \                                                             
  --env OMPI_ALLOW_RUN_AS_ROOT=1 \                                              
  --env OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1 \                                      
  docker.io/stephey/orca:3.0 ./orca input_parallel.inp
```

## Long-Term Location for ORCA Executables

The ORCA executables are about 40GB. To store them properly, a user will
need to contact DAS to increase the storage capacity of
`/global/common/software`.
