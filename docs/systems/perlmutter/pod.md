# Perlmutter On Demand

Perlmutter On Demand (POD) is a collection of resources dedicated
to support the upcoming Integrated Research Infrastructure (IRI)
effort of DOE. Currently, POD consists of

* One cabinet of 256 CPU nodes. These are identical to 
  [Perlmutter's CPU nodes](./architecture.md/#cpu-nodes).
* One cabinet of 128 GPU nodes. These GPU nodes have 512 GB 
  of RAM and 80 GB of GPU memory. They are similar to
  [Perlmutter's GPU nodes](./architecture.md/#gpu-nodes) with 
  80GB A100 GPUs except they have twice the RAM.
* 3PB of SCRATCH which is an all-flash Lustre filesystem.
  **This filesystem is not yet available to users**.

## Current Status of POD

Currently, the POD compute nodes POD nodes have been integrated
into Perlmutter and are dedicated to urgent 
computing, i.e. the interactive, Jupyter, and realtime QOSes, 
as well as reservations. By adding dedicated resources for 
interactive and urgent jobs we expect not only for those 
jobs to start sooner but also to improve turnaround for jobs 
in other partitions. As plans evolve for IRI, the usage of POD 
may evolve as well.

## Impact of POD

* When using interactive, Jupyter, or realtime QOS 
  chances are high that the job will be directed to a POD node. 
  We expect the extra nodes to make the interactive/realtime
  experience faster and smoother. Allocation of a POD node will 
  be taken care of behind the scenes, as usual.
* When using any other queue, nothing will change except 
  we expect jobs to be scheduled a little faster. 
  Large jobs should see the highest improvement.

!!! tip
    For now, you can tell if your job ran on a POD
    node by looking at the node list in slurm, POD node IDs start 
    with `nid2*`.

!!! alert
    A POD GPU node has 512GB of RAM which is twice the amount of a
    any Perlmutter GPU node. 
    Since urgent jobs will preferentially be scheduled on these nodes,
    the job could end up with a node with extra RAM without 
    requesting it.  If you want to test your code with a more limited 
    memory footprint, you can add the `--mem=256G` to your batch script.
