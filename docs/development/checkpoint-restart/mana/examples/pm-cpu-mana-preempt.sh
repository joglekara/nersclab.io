#!/bin/bash
#SBATCH -J test_cr
#SBATCH -q preempt
#SBATCH -N 2 
#SBATCH -C cpu
#SBATCH -t 24:00:00
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err

#c/r with mana 
module load mana 

#checkpointing once every hour
mana_coordinator -i 3600

#running under mana control
srun -n 64 mana_launch ./a.out
