# IDL

IDL's primary use is in the analysis and display of scientific data through its
programming, plotting, and image display facilities.

## Availability at NERSC

We currently support IDL 8.5 (default) and IDL 8.9 on Perlmutter.
During the AY 2024 rollover, we plan to change IDL 8.9 to the default.
If you have any questions or concerns, please don't hesitate
to [file a ticket](https://help.nersc.gov).

## Using IDL at NERSC

First load the IDL module

```shell
module load idl
```

and start IDL by running

```shell
idl
```

Or to use the IDL development environment, first log on to Perlmutter via
[NoMachine](../../connect/nx.md) and then type:

```shell
module load idl
idlde
```

IDL has a rich set of demos. To view them, in the **IDL command line**, type:

```shell
demo
```

## Running IDL procedures via batch job

If you would prefer to run IDL via a batch job, here is a simple
demonstration workflow.

Let's assume we would like to run an IDL
procedure called `hello_idl.pro`:

```shell
pro idl_hello
print, 'Hello world from IDL'
end
```

Here is the batch script that will run our IDL procedure which we will call
`idl_batch.sh`. For more help setting up your batch script, check out our
[jobscript generator](../../jobs/jobscript-generator.md).

```slurm
#!/bin/bash
#SBATCH -N 1
#SBATCH -C cpu
#SBATCH -q regular
#SBATCH -t 00:05:00

#load the idl module
module load idl

#run the application:
idl -e "idl_hello"
```

Then to submit your idl job, simply type

```shell
sbatch idl_batch.sh
```

`idl_batch.sh` will load IDL and execute the procedure that you specified. The
key is the `idl -e "idl_hello"` which allows IDL to execute your procedure via
the command line.

For more information about multithreading in IDL see the
[IDL multi-threading FAQ](https://www.nv5geospatialsoftware.com/Support/Maintenance-Detail/ArtMID/13350/ArticleID/19291/3252).

## Additional resources

For more information on IDL, please refer to the
[IDL software home page](https://www.nv5geospatialsoftware.com/Products/IDL).
