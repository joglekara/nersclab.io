# CP2K

CP2K is a quantum chemistry and solid state physics software package
that can perform atomistic simulations of solid state, liquid,
molecular, periodic, material, crystal, and biological systems.

## Availability

| System     | Architecture | Modulefile | Image                            |
|------------|--------------|------------|----------------------------------|
| Perlmutter | GPU/A100     | N/A        | `docker:nvcr.io/hpc/cp2k:v9.1.0` |
| Perlmutter | CPU/Milan    | N/A        | `docker:cp2k/cp2k:2022.1`        |

!!! warning "MPI Support on CPU"
	For MPI support for images from
	[dockerhub](https://hub.docker.com/r/cp2k/cp2k/tags) use
	`dev20220519` and newer images.
	
!!! warning "MPI Performance on Perlmutter GPU"
	The `docker:nvcr.io/hpc/cp2k:v9.1.0` image has MPI support but
	multinode performance may not be optimal due to MPI ABI issues that
	are being actively addressed.

## Support

The [CP2K Reference Manual](https://manual.cp2k.org/) provides details
on how to setup calculations and the various options available.

For questions about CP2K usage that are not specific to NERSC please
consult the [CP2K Forum](https://groups.google.com/group/cp2k) and
[CP2K FAQ](https://www.cp2k.org/faq).

If you need to make your own customized build of CP2K the [Makefile
and build
script](https://gitlab.com/NERSC/nersc-user-software/-/tree/master/manual-build-scripts/cp2k)
used to create NERSC's modules are available.

## CP2K at NERSC

### Perlmutter

#### CPU

```slurm
#!/bin/bash 
#SBATCH --image docker:cp2k/cp2k:2022.1
#SBATCH --nodes 1 
#SBATCH --cpus-per-task 2 
#SBATCH --ntasks-per-node 128 
#SBATCH --constraint cpu 
#SBATCH --qos debug 
#SBATCH --time-min 5 
#SBATCH --time 30 

srun shifter --entrypoint cp2k -i H2O-64.inp 
```

#### GPU

```slurm
#!/bin/bash
#SBATCH --image docker:nvcr.io/hpc/cp2k:v9.1.0
#SBATCH --nodes 1
#SBATCH --cpus-per-task 32
#SBATCH --gpus-per-task 1
#SBATCH --ntasks-per-node 4
#SBATCH --constraint gpu
#SBATCH --qos debug
#SBATCH --time-min 5
#SBATCH --time 30

export OMP_NUM_THREADS=16
srun --cpu-bind cores --mpi pmi2 --module gpu shifter --entrypoint cp2k -i H2O-256.inp
```

## Performance

Performance of CP2K can vary depending on the system size and run type. The 
multi-node scaling performance of the code depends on the amount of work (or 
number of atoms) per MPI rank. It is recommended to try a representative 
test case on different number of nodes to see what gives the best performance.

## User Contributed Information

* User contributions (tips/etc) are welcome!
